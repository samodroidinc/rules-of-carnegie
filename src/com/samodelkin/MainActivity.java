package com.samodelkin;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ScrollView;

public class MainActivity extends Activity implements View.OnClickListener
{
    private boolean isShowingMainMenu; //показ главного меню
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.main);
        isShowingMainMenu=true;
    }
    
    public void onClick(View v){
        switch(v.getId()){
            case R.id.button1:setContentView(R.layout.rules1);
                isShowingMainMenu=false;
                break;
            case R.id.button2:setContentView(R.layout.rules2);
                isShowingMainMenu=false;
                break;
            case R.id.button3:setContentView(R.layout.rules3);
                isShowingMainMenu=false;
                break;    
            case R.id.button4:setContentView(R.layout.rules4);
                isShowingMainMenu=false;
                break;    
            case R.id.button5:setContentView(R.layout.rules5);
                isShowingMainMenu=false;
                break;    
            case R.id.button6:setContentView(R.layout.rules6);
                isShowingMainMenu=false;
                break;    
            case R.id.button7:setContentView(R.layout.rules7);
                isShowingMainMenu=false;
                break;    
            case R.id.button8:setContentView(R.layout.rules8);
                isShowingMainMenu=false;
                break;    
            case R.id.button9:setContentView(R.layout.rules9);
                isShowingMainMenu=false;
                break;    
            case R.id.button10:setContentView(R.layout.rules10);
                isShowingMainMenu=false;
                break;    
        }
    }
    
    public void onBackPressed(){
         if (!isShowingMainMenu){
         setContentView(R.layout.main);
         isShowingMainMenu=true;
         }
         else finish();
    }
}
